-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 29 Mars 2019 à 18:44
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `testwpcms`
--

-- --------------------------------------------------------

--
-- Structure de la table `content`
--

CREATE TABLE `content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` varchar(20) NOT NULL DEFAULT 'deactivate',
  `lang` varchar(8) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'page',
  `tpl` varchar(80) NOT NULL,
  `url` varchar(60) DEFAULT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(160) DEFAULT NULL,
  `content` longtext,
  `user_update` bigint(20) UNSIGNED DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `user_insert` bigint(20) UNSIGNED NOT NULL,
  `date_insert` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `content`
--

INSERT INTO `content` (`id`, `state`, `lang`, `type`, `tpl`, `url`, `title`, `description`, `content`, `user_update`, `date_update`, `user_insert`, `date_insert`) VALUES
(1, 'active', 'fr', 'page', 'home', 'home', 'Test WP CMS', '', '{\"titre-home\":\"Accueil\",\"texte-home\":\"Bienvenue sur votre nouveau site&nbsp;! Pour modifier cette page, cliquez sur le lien de modification. Pour plus d’informations sur la personnalisation de votre site, visitez le site <a href=\\\"http:\\/\\/learn.wordpress.com\\/\\\">http:\\/\\/learn.wordpress.com\\/<\\/a>.\",\"img-home\":\"media\\/skyline-buildings-new-york-skyscrapers.jpg?1553767937\"}', 1, '2019-03-29 18:34:24', 1, '2019-03-28 09:56:21'),
(2, 'active', 'fr', 'page', 'contact', 'contact', 'Contact', '', '{\"titre-contact\":\"Contact\",\"texte-formulaire\":\"Ceci est une page de coordonnées comportant quelques informations de base et un formulaire de contact.&nbsp;\"}', 1, '2019-03-28 10:31:14', 1, '2019-03-28 10:07:06'),
(3, 'active', 'fr', 'page', 'page', 'a-propos', 'A propos', '', '{\"title\":\"A propos\",\"texte\":\"Site test du thème Twenty nineteen avec le CMS Translucide\"}', 1, '2019-03-28 10:51:11', 1, '2019-03-28 10:51:02');

-- --------------------------------------------------------

--
-- Structure de la table `meta`
--

CREATE TABLE `meta` (
  `id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(32) NOT NULL,
  `cle` varchar(255) NOT NULL DEFAULT '',
  `val` text NOT NULL,
  `ordre` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `meta`
--

INSERT INTO `meta` (`id`, `type`, `cle`, `val`, `ordre`) VALUES
(0, 'header', 'fr', '{\"titre\":\"Twenty Nineteen\",\"texte\":\"Site test du thème Twenty nineteen avec le CMS Translucide\",\"logo\":\"media\\/resize\\/selection-002-150x112.png?zoom=media\\/selection-002.png&1553767887\"}', 0),
(0, 'nav', 'fr', '{\"0-0\":{\"href\":\"home\",\"text\":\"Accueil\",\"id\":\"\",\"class\":\"selected editable\",\"target\":\"\"},\"1-0\":{\"href\":\"contact\",\"text\":\"Contact\",\"id\":\"\",\"class\":\"editable\",\"target\":\"\"},\"2-0\":{\"href\":\"a-propos\",\"text\":\"A propos\",\"id\":\"\",\"class\":\"editable\",\"target\":\"\"}}', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` varchar(20) NOT NULL DEFAULT 'active',
  `auth` varchar(255) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `info` text,
  `password` char(64) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `oauth` text,
  `token` varchar(255) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_insert` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `state`, `auth`, `name`, `email`, `info`, `password`, `salt`, `oauth`, `token`, `date_update`, `date_insert`) VALUES
(1, 'active', 'edit-admin,edit-user,edit-config,edit-nav,edit-header,edit-footer,add-media,add-page,add-article,add-event,add-product,edit-media,edit-page,edit-article,edit-event,edit-product,add-media-public,edit-public', NULL, 'stefaniel@lilo.org', NULL, '54015a13015c06e6b7d7902f8bab4067f4265eb1eafa2420d7acc2db544816ef', '625df492624a4cf0', NULL, '6c8be765f45248e82c856725ebefd2586105d86742ff435bc39adcbcff1e4cf7', NULL, '2019-03-28 09:56:21');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`),
  ADD KEY `state` (`state`),
  ADD KEY `type` (`type`),
  ADD KEY `lang` (`lang`);

--
-- Index pour la table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`,`type`,`cle`),
  ADD KEY `type` (`type`,`cle`),
  ADD KEY `ordre` (`ordre`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `state` (`state`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `content`
--
ALTER TABLE `content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
